<?php

namespace Vetrov\Config;

class ConfigFidner
{
    private static $local = '/local/php_interface/config';
    private static $template = '/local/templates';

    public static function global(): array
    {
        return static::configs($_SERVER['DOCUMENT_ROOT'] . static::$local);
    }

    public static function template(): array
    {
        return static::configs($_SERVER['DOCUMENT_ROOT'] . static::$template . '/' . SITE_TEMPLATE_PATH . '/config');
    }

    private static function configs($path): array
    {
        if (! file_exists($path) || ! scandir($path)) {
            return [];
        }

        return static::getFilesContent($path);
    }

    private static function getFilesContent($pathToDir): array
    {
        $content = [];

        foreach (array_diff(scandir($pathToDir), ['.', '..']) as $item) {
            if (! is_file($pathToDir . '/' . $item)) {
                continue;
            }

            $content[str_replace('.php', '', $item)] = include($pathToDir . '/' . $item);
        }

        return $content;
    }

}