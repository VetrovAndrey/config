<?php

namespace Vetrov\Config;

class Config
{
    private static $instance;

    private $configs = [];

    public static function getInstance()
    {
        if (is_null(static::$instance)) {
            static::$instance = new self();
        }

        return static::$instance;
    }

    public static function get($pathToConfig)
    {
        $instance = self::getInstance();

        return $instance->getConfig(explode('.', $pathToConfig));
    }

    public function getConfig($pathToConfig)
    {
        $result = $this->configs;

        foreach ($pathToConfig as $item) {
            if (empty($result[$item]) || ! isset($result[$item])) {
                throw new \Exception('config not found');
            }

            $result = $result[$item];
        }

        return $result;
    }

    private function __construct()
    {
        $this->configs['global'] = ConfigFidner::global();
        $this->configs['template'] = ConfigFidner::template();
    }

    private function __clone()
    {
    }
}
